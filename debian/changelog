flwm (1.02+git2015.10.03+7dbb30-7) unstable; urgency=medium

  * Fix typo in description of 06-create-dirs.diff.
  * Update Vcs-* headers for move to Salsa.
  * Declare compliance with Debian Policy 4.5.0. (No changes needed.)
  * Mention in debian/watch why switching to HTTPS does not help here.
  * Bump debhelper compatibility level to 13.
    + Build-depend on "debhelper-compat (= 13)" to replace debian/compat.

 -- Axel Beckert <abe@debian.org>  Sun, 19 Jul 2020 05:41:44 +0200

flwm (1.02+git2015.10.03+7dbb30-6) unstable; urgency=high

  * Fix generate-flwm-menu-from-desktop-files to handle non-existing
    /usr/share/applications/ gracefully. (Closes: #886935)

 -- Axel Beckert <abe@debian.org>  Thu, 11 Jan 2018 23:34:36 +0100

flwm (1.02+git2015.10.03+7dbb30-5) unstable; urgency=medium

  * Fix File::DesktopEntry's package name also in package description.
  * Fix typo in 1.02+git2015.10.03+7dbb30-2 entry.
  * Bump debhelper compatibility level to 11.
    + Update versioned debhelper build-dependency accordingly.
  * Declare compliance with Debian Policy 4.1.3. (No changes needed.)

 -- Axel Beckert <abe@debian.org>  Sat, 30 Dec 2017 05:10:19 +0100

flwm (1.02+git2015.10.03+7dbb30-4) unstable; urgency=medium

  * Explain in package description which packages are needed to generate
    which flwm menu.
  * Fix package name of File::DesktopEntry (libfile-desktop-entry-perl vs
    libfile-desktopentry-perl).

 -- Axel Beckert <abe@debian.org>  Sat, 23 Dec 2017 03:37:04 +0100

flwm (1.02+git2015.10.03+7dbb30-3) unstable; urgency=medium

  * Harden generate-flwm-menu-from-desktop-files against more kinds of
    broken .desktop files to avoid upgrade issues.
  * Add /usr/games/ to $PATH inside generate-flwm-menu-from-desktop-files,
    otherwise it skips all games in /usr/games/ as /usr/games usually
    isn't in root's $PATH.

 -- Axel Beckert <abe@debian.org>  Sat, 23 Dec 2017 03:02:02 +0100

flwm (1.02+git2015.10.03+7dbb30-2) unstable; urgency=medium

  * Set "Rules-Requires-Root: no".
  * Declare compliance with Debian Policy 4.1.2.
    + Update DEP5 format URL in debian/copyright to HTTPS.
  * Drop obsolete debian/source/local-options.
  * Drop debian/flwm-dbg.lintian-overrides, obsoleted by previous upload.
  * Now that the package only produces one binary package, rename
    debian/flwm.{wm,lintian-overrides} to debian/{wm,lintian-overrides}.
  * Generate FLWM menu also from .desktop files.
    + Trigger menu regeneration upon /usr/share/applications/ changes.
    + Add required Perl modules for that functionality as Recommends and
      guard the trigger by a check for availability.
  * Move --dbgsym-migration to dh, allowing to drop the dh_strip override.

 -- Axel Beckert <abe@debian.org>  Fri, 22 Dec 2017 02:14:23 +0100

flwm (1.02+git2015.10.03+7dbb30-1) unstable; urgency=medium

  * Import new upstream snapshot (commit 7dbb30, 3rd of October 2015)
    + Fixes crash on untitled windows.
    + Refresh patch 06-fix-ftbfs-with-active-color.diff.
  * Drop manual -dbg package in favour of an automatic -dbgsym package.
  * Switch Vcs-* headers to https:// and cgit web interface.
  * Enable all hardening build flags.
  * Update multiple URLs in debian/upstream/metadata.
  * Disable DH_VERBOSE again.
  * Change dh_strip parameter --ddeb-migration to --dbgsym-migration
  * Bump debhelper compatibility to 10
    + Update versioned debhelper build-dependency.
    + Drop --parallel and --with autoreconf from debian/rules.
    + Drop explicit build-dependency on dh-autoreconf.
  * Declare compliance with Debian Policy 3.9.8.

 -- Axel Beckert <abe@debian.org>  Tue, 13 Sep 2016 00:03:13 +0200

flwm (1.02+git2015.04.29-1) unstable; urgency=low

  * New upstream git snapshot with FLTK 1.3 support.
    + Drop patch 03-parantheses-warnings.diff, obsoleted by upstream.
    + Refresh remaining patches where needed.
    + Build-depend on libfltk1.3-dev instead of libfltk1.1-dev
  * Declare compliance with Debian Policy 3.9.6. (No changes needed.)
  * Convert debian/copyright to machine-readable DEP5 format and update
    copyright owners and years.

 -- Axel Beckert <abe@debian.org>  Thu, 30 Jul 2015 20:52:49 +0200

flwm (1.02+cvs20080422-11) unstable; urgency=medium

  * Bump Standards-Version to 3.9.5 (no changes)
  * Use dh_auto_clean instead of "make clean" + manual "rm -rf"
  * Enable parallel building
  * Spell "Windows" correctly and neutral also in the long package
    description of flwm-dbg.
  * Drop obsolete man page removal in build target.
  * Add a debian/upstream/metadata file according to DEP-12.

 -- Axel Beckert <abe@debian.org>  Sat, 28 Jun 2014 17:27:11 +0200

flwm (1.02+cvs20080422-10) unstable; urgency=low

  * Bump debhelper compatibility to 9
    + Update versioned debhelper build-dependency
  * Switch to a dh7 style debian/rules file:
    + Use dh_auto_{configure,build,install} and implicit dpkg-buildflags
    + Use dh_clean and debian/clean instead of manual "rm" where possible
    + Use debian/wm instead parameter to dh_installwm
    + Drop get-orig-source target
  * Use dh_autoreconf
    + Add build-dependency on dh-autoreconf
  * Pass CPPFLAGS inside CXXFLAGS to fix lintian warning
    hardening-no-fortify-functions.
    + Add build-dependency on dpkg-dev >= 1.16.1~
  * Add flwm.desktop session file. Thanks Luca Capello! (Closes: #635044)
  * Add patch by Stephen Carrier to fix FTBFS if ACTIVE_COLOR is defined
    (i.e. with modified config.h; Closes: #698021)
  * Bump Standards-Version to 3.9.4 (no changes)
  * Add lintian-overrides for no-upstream-changelog
  * Apply wrap-and-sort

 -- Axel Beckert <abe@debian.org>  Tue, 28 May 2013 22:54:21 +0200

flwm (1.02+cvs20080422-9) unstable; urgency=low

  * New Maintainer (Closes: #622165)
  * Spell "Windows" correctly and neutral in the package description.
  * Fix lintian warning debian-rules-missing-recommended-target.
  * Add abort-on-upstream-changes and unapply-patches to
    debian/source/local-options as source format 3.0 stubbornly does not
    allow to put it into debian/source/options
  * Add Vcs-* headers.

 -- Axel Beckert <abe@debian.org>  Fri, 22 Jul 2011 02:38:26 +0200

flwm (1.02+cvs20080422-8) unstable; urgency=low

  * Bump Standards-Version to 3.9.2 (no changes needed).
  * Set the maintainer to the Debian QA Group.

 -- Patrick Matthäi <pmatthaei@debian.org>  Sun, 10 Apr 2011 18:15:09 +0200

flwm (1.02+cvs20080422-7) unstable; urgency=low

  * Bump Standards-Version to 3.9.1 (no changes needed).
  * Convert package to the 3.0 (quilt) format.

 -- Patrick Matthäi <pmatthaei@debian.org>  Sun, 06 Feb 2011 19:06:26 +0100

flwm (1.02+cvs20080422-6) unstable; urgency=low

  * Fix typo in get-orig-source target.
  * Refer in debian/copyright to the GPL-2 file instead of GPL.
  * Add my own copyright for the Debian packaging.
  * Update to compat 7.
  * Use the new dh_installwm for registering flwm as one.

 -- Patrick Matthäi <pmatthaei@debian.org>  Thu, 14 May 2009 12:12:25 +0200

flwm (1.02+cvs20080422-5) unstable; urgency=low

  * Add missing ${misc:Depends}. Thanks lintian.
  * Mangle cvs version tag in the Debian version.
  * Add get-orig-source target.
  * Bump Standards-Version to 3.8.1 (no changes needed).
  * Change section of flwm-dbg to debug.
  * Change my email address.
  * Remove DM-Upload-Allowed control field.
  * Fix lintian information using-first-person-in-description.

 -- Patrick Matthäi <pmatthaei@debian.org>  Mon, 20 Apr 2009 19:45:22 +0200

flwm (1.02+cvs20080422-4) unstable; urgency=low

  * Uploading to unstable.

 -- Patrick Matthäi <patrick.matthaei@web.de>  Tue, 14 Oct 2008 20:33:47 +0200

flwm (1.02+cvs20080422-3) experimental; urgency=low

  * Uploading to experimental, because Lenny is frozen.
  * Do not call x-window-manager with path in postinst.
    Thanks lintian.
  * Bump Standards-Version to 3.8.0.
    - Added README.source.
  * Removed debian/flwm.1 and added patches/04-manpage-fixes.dpatch instead of
    the whole manpage.
  * Added 05-remove-unneeded-linking.dpatch which removes some useless linking.
  * Added 06-create-dirs.dpatch which also creates directorys at install time.
    - Drop debian/dirs.
  * Drop deprecated README.Debian.

 -- Patrick Matthäi <patrick.matthaei@web.de>  Sat,  6 Sep 2008 10:06:19 +0200

flwm (1.02+cvs20080422-2) unstable; urgency=low

  * Added 03-parantheses-warnings.dpatch. It adds some parantheses in the code
    and avoids so on some warnings from the compiler.
  * Some minor header fixes for the 01 and 02 dpatch.

 -- Patrick Matthäi <patrick.matthaei@web.de>  Wed, 14 May 2008 14:56:40 +0100

flwm (1.02+cvs20080422-1) unstable; urgency=low

  * New maintainer.
    Closes: #369933
  * New CVS snapshot from the 22th April 2008.
    Closes: #458946, #272672, #316885, #316209
  * Reworked the whole package with debhelper. This also fixes all lintian
    warnings.

 -- Patrick Matthäi <patrick.matthaei@web.de>  Tue,  3 May 2008 20:32:21 +0100

flwm (1.02-3) unstable; urgency=low

  * debian/control:
    + Bump versioned build-dep on libfltk1.1-dev to 1.1.7-6.
      Closes: #450551.

 -- Bill Allombert <ballombe@debian.org>  Mon, 12 Nov 2007 13:14:50 +0100

flwm (1.02-2) unstable; urgency=low

  * Add patch 201_nostrip:
    + Remove -s from install so that nostrip actually works.
      Closes: #436844.  Thanks Julien Danjou.
  * debian/menu: update menu section to "Window Managers".
  * debian/rules: Remove - before make clean
  * debian/control: Recommends menu. Closes: #406277.

 -- Bill Allombert <ballombe@debian.org>  Sun, 12 Aug 2007 09:55:42 +0200

flwm (1.02-1) unstable; urgency=low

  * New upstream release
   + Remove 100_double_ampersand applied upstream.
   + Otherwise this is identical to 1.01-1.

 -- Bill Allombert <ballombe@debian.org>  Wed, 13 Sep 2006 12:29:11 +0200

flwm (1.01-1) unstable; urgency=low

  * New upstream release
    + This release catch the release of the Alt key again. Closes: #246089.
    + The following patches were applied upstream (Thanks Bill Spitzak).
      100_fl_filename_name 101_visible_focus 102_charstruct 103_man_typo
      104_g++-4.1_warning 105_double_ampersand 201_background_color
    + Add 100_double_ampersand to fix a typo in this release.
  * debian/watch: added.

 -- Bill Allombert <ballombe@debian.org>  Fri, 30 Jun 2006 01:17:06 +0200

flwm (1.00-10) unstable; urgency=low

  * Add patch 104_g++-4.1_warning that fix five warnings.
  * Add patch 105_double_ampersand that fix handling of '&' in window titles
    in the windows list.  Closes: #358337. Thanks Silas S. Brown.
  * Rediff Debian patches to compensate.
  * Bump Standards-Version to 3.7.2.

 -- Bill Allombert <ballombe@debian.org>  Sat, 10 Jun 2006 22:14:18 +0200

flwm (1.00-9) unstable; urgency=low

  * debian/menu-method:
    - Add removemenu config option
    - Use #! /usr/bin/install-menu
  * debian/rules: Fix handling of nostrip.
  * Use dpatch in pre-applied mode.
  * Add patch 201_background_color that should fix the color options
    Closes: #267983. Thanks Nils Krueger.

 -- Bill Allombert <ballombe@debian.org>  Tue, 28 Feb 2006 22:28:20 +0100

flwm (1.00-8) unstable; urgency=low

  * Rebuild with current g++/libfltk1.1. Closes: #328174.
  * debian/control:
    + Bump Standard-Version to 3.6.2.
    + Remove menu versioned conflict since Sarge was released.
    + Bump versioned build-dep on libfltk1.1-dev to 1.1.6-7.
  * debian/rules:
    + Move menu file to /usr/share/menu.
    + No more install debian/README.debian.
  * debian/copyright: Update FSF address.
  * debian/flwm.1: Remove useless blurb about RedHat.
  * debian/menu-method: Replace /usr/doc/menu by /usr/share/doc/menu.
  * Move Debian menu to /var/lib/flwm/wmx.
  * debian/postinst:
    + Remove legacy register-window-manager logic.
    + Remove old /etc/X11/flwm menu files.

 -- Bill Allombert <ballombe@debian.org>  Thu, 15 Sep 2005 14:57:50 +0200

flwm (1.00-7) unstable; urgency=low

  * Rebuild with g++ with correct ABI.
  * Add Fl::visible_focus(0) in initialize(). This should fix
    Alt-Enter with GNOME2 apps. Closes: #246090. Thanks Duncan Sargeant
  * Ship the menu-method not executable in the tarball.

 -- Bill Allombert <ballombe@debian.org>  Fri,  8 Apr 2005 12:37:28 +0200

flwm (1.00-6) unstable; urgency=low

  * New maintainer
  * debian/menu:
     + Capitalize the title.
     + Quote the needs.
     + Add a longtitle.
  * debian/postinst, debian/postrm:
    + Remove spurious call to wm-menu-config. Closes: #296891.
    + Clean up the code that handle the menu-methods permissions.
  * debian/shlibs.local: obsolete, removed.
  * debian/menumethod: quote treewalk
  * debian/control: bump Standard-Version to 3.6.1
  * debian/copyright: mention the change of maintainer.
  * debian/rules: implement DEB_BUILD_OPTIONS noopt and nostrip.
  * Acknowledge previous NMU:
    - Closes: #201205: Patch from BSP NMU
    - Closes: #191982: flwm: Uninstallable in sid
    - Closes: #196400: flwm: FTBFS: `fl_filename_name' undeclared
    - Closes: #190389: flwm: Segmentation fault on startup
    - Closes: #199435: flwm: Upstream location is outdated

 -- Bill Allombert <ballombe@debian.org>  Sat,  5 Mar 2005 15:35:49 +0100

flwm (1.00-5.1) unstable; urgency=low

  * NMU (playing at home the Debcamp BSP).
  * debian/menu-method:
     + prerun: Add -depth to ensure we delete the children before the
       directory.
     + Add outputencoding.
  * debian/postinst, debian/prerm: remove /usr/share/doc transition code.
  * debian/control:
     + Conflicts with menu (<< 2.1.8-1) since menu-methods use $basesection.
     + In fact Conflicts with menu (<< 2.1.9-1) since menu-methods use
       outputencoding.
     + Build-Depends: Remove '| libfltk-dev' since flwm does not build
       with libfltk1-dev. Closes: #196400.
     + Build-Depends: Add libfltk1.1-dev (>= 1.1.3-2.1) to force
       linkage with libXft.so.2. Closes: #191982.
     + Build-Depends: In fact, add libfltk1.1-dev (>= 1.1.3+1.1.4rc1-3) to
       avoid libfltk version where fl_xfont is NULL. Closes: #190389.
       Thanks Aaron Ucko.
  * debian/copyright:
     + Add new web site URL. Closes: #199435.  Thanks Adrian Bunk.
     + Change Author(s): to Author: to make lintian happy.

 -- Bill Allombert <ballombe@debian.org>  Fri, 20 Jun 2003 14:43:16 +0200

flwm (1.00-5) unstable; urgency=low

  * Finally, menu has the features needed to support slashes in menu entry
    titles (see bug#136947). Closes: #120376.
  * Get rid of the mesag-dev build-dependecy, it seems libfltk now works
    even without explicit -lGL. Closes: #156336.
  * Switch to using libfltk1.1, requiring a radical three-letter fix to
    main.C
  * Support fonts for which fontstruct->per_char is NULL. Used to segfault
    when rotating such title fonts.
  * Clean up description. Closes: #124621.
  * Remove local variables from debian/changelog.
  * Make menu-methods not fail when there is no ~/.wmx. Closes: #134900.

 -- Tommi Virtanen <tv@debian.org>  Sat, 19 Apr 2003 22:31:54 +0300

flwm (1.00-4) unstable; urgency=low

  * Added xutils to Build-Deps. Closes: #87825.

 -- Tommi Virtanen <tv@debian.org>  Thu,  1 Mar 2001 22:10:41 +0200

flwm (1.00-3) unstable; urgency=low

  * Build-depend on libfltk1-dev | libfltk-dev. Closes: #87315.

 -- Tommi Virtanen <tv@debian.org>  Sat, 24 Feb 2001 14:24:53 +0200

flwm (1.00-2) unstable; urgency=low

  * Moved out of /usr/X11R6.
  * Postinst had an embarassing logic error that made it
    not create the /usr/doc link if register-window-manager
    was not found.

 -- Tommi Virtanen <tv@debian.org>  Fri, 23 Feb 2001 01:00:32 +0200

flwm (1.00-1) unstable; urgency=low

  * New upstream version.
  * Use dpkg-gencontrol -isp.
  * Upstream fixed i18n chars in title bars. Closes: #81839.

 -- Tommi Virtanen <tv@debian.org>  Sat, 27 Jan 2001 02:04:44 +0200

flwm (0.25-8) unstable; urgency=low

  * _Again_ recompile to get rid of bad libgl dependency. Duh.
  * Changed -lMesaGL to -lGL.
  * Added shlibs.local to make svgalib1g dependency also have
    svgalib-dummyg1.

 -- Tommi Virtanen <tv@debian.org>  Sat, 15 Jan 2000 12:51:52 +0200

flwm (0.25-7) unstable; urgency=low

  * Remove libstdc++2.10-dev from build deps, as it is build-essential due
    to g++ depending on it. Closes: #54020.

 -- Tommi Virtanen <tv@debian.org>  Tue,  4 Jan 2000 18:30:10 +0200

flwm (0.25-6) unstable; urgency=low

  * Recompile to get rid of the bad libgl dependency.

 -- Tommi Virtanen <tv@debian.org>  Sun,  2 Jan 2000 19:10:52 +0200

flwm (0.25-5) unstable; urgency=low

  * Change x-window-manager alternative priority to 40.

 -- Tommi Virtanen <tv@debian.org>  Tue, 28 Dec 1999 21:15:33 +0200

flwm (0.25-4) unstable; urgency=low

  * Seems like I forgot debhelper from builddeps.
    Fixed this by rewriting rules not to use debhelper ;)

 -- Tommi Virtanen <tv@debian.org>  Sat,  4 Dec 1999 14:23:15 +0200

flwm (0.25-3) unstable; urgency=low

  * Use LIBS="-lfltk -lMesaGL" in debian/rules. Closes: #51690.
  * FHS.
  * Build-deps.
  * Provide x-window-manager, use update-alternatives,
    don't use dh_installwm. Make sure we remove all traces
    of register-window-manager.
  * Remove /etc/X11/flwm on purge. Closes: #49210.
  * Let debhelper take care of the maintainer scripts.

 -- Tommi Virtanen <tv@debian.org>  Sat,  4 Dec 1999 00:48:39 +0200

flwm (0.25-2) unstable; urgency=low

  * Incorporated JHM's patches; menu support and miscellanous fixes.
    Thanks JHM. Closes: #43686.

 -- Tommi Virtanen <tv@debian.org>  Sun, 29 Aug 1999 14:49:59 +0300

flwm (0.25-1.1) unstable; urgency=low

  * Added menu support:
    * Menu.C: use a global menu /etc/X11/flwm/wmx if there is no ~/.wmx .
    * Wrote /etc/menu-methods/flwm .
    * Updated flwm.1 .
    * Added postinst and postrm; added Suggests: menu (>=1.5).
  * Build -g.
  * Minor description update.

 -- J.H.M. Dassen (Ray) <jdassen@wi.LeidenUniv.nl>  Sun, 29 Aug 1999 08:42:47 +0200

flwm (0.25-1) unstable; urgency=low

  * New upstream source

 -- Tommi Virtanen <tv@debian.org>  Wed, 25 Aug 1999 00:05:40 +0300

flwm (0.24-2) unstable; urgency=low

  * Added flwm to /etc/X11/window-managers. Closes bug #37117.

 -- Tommi Virtanen <tv@debian.org>  Sun, 30 May 1999 21:47:41 +0300

flwm (0.24-1) unstable; urgency=low

  * New upstream source

 -- Tommi Virtanen <tv@debian.org>  Sun, 16 May 1999 20:40:02 +0300

flwm (0.23-1) unstable; urgency=low

  * New upstream release

 -- Tommi Virtanen <tv@debian.org>  Tue, 27 Apr 1999 13:59:21 +0300

flwm (0.22-1) unstable; urgency=low

  * New upstream release
  * Moved to /usr/X11R6

 -- Tommi Virtanen <tv@debian.org>  Tue, 20 Apr 1999 01:06:13 +0300

flwm (0.19-2) unstable; urgency=low

  * Cleaned the .diff.gz by removing extra files in clean
  * Added a menu file

 -- Tommi Virtanen <tv@debian.org>  Tue,  9 Mar 1999 18:47:19 +0200

flwm (0.19-1) unstable; urgency=low

  * Initial Release.

 -- Tommi Virtanen <tv@debian.org>  Tue,  9 Mar 1999 14:18:03 +0200
